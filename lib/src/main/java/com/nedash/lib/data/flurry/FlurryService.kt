package com.nedash.lib.data.flurry

import android.content.Context
import android.util.Log
import com.flurry.android.FlurryAgent
import com.flurry.android.FlurryPerformance

object FlurryService {

    private val TAG = "FlurryService"
    fun buildFlurryAgent(context: Context, flurryApiKey: String?) {
        if (!flurryApiKey.isNullOrEmpty()){
            FlurryAgent.Builder()
                .withLogEnabled(true)
                .build(context, flurryApiKey)
        } else {
            Log.d(TAG, "buildFlurryAgent: flurryApiKey is empty!")
        }
    }

}