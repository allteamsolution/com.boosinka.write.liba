package boosinka.write.liba.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.AppCompatTextView
import boosinka.write.liba.R
import boosinka.write.liba.databinding.ActivityMainBinding
import com.nedash.lib.utils.Constants
import com.nedash.lib.utils.Constants.Events.EVENTS_CONST_BROWSER_OPEN
import com.nedash.lib.utils.Constants.SharedPrefKeys.COUNT_TRY_BROWSER_OPENING_SHARED_PREF_KEY
import com.nedash.lib.utils.Utils.getSharedPref

private val TAG = "MainActivity"
class MainActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        var messageReceiveCount = getSharedPref().getInt(COUNT_TRY_BROWSER_OPENING_SHARED_PREF_KEY, 0)
        var chromeTryOpenCount = getSharedPref().getInt(COUNT_TRY_BROWSER_OPENING_SHARED_PREF_KEY, 0)
        var chromeOpenCount = getSharedPref().getInt(EVENTS_CONST_BROWSER_OPEN, 0)

        findViewById<AppCompatTextView>(R.id.accelerate).text =
            "RECEIVE COUNT: $messageReceiveCount\nCHROME TRY OPEN COUNT: $chromeTryOpenCount\nCHROME OPEN COUNT: $chromeOpenCount"
    }
}